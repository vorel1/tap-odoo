"""Tests standard tap features using the built-in SDK tests library."""

import datetime

from singer_sdk.testing import get_standard_tap_tests

from tap_odoo.tap import TapOdoo

SAMPLE_CONFIG = {
    "start_date": datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d"),
    "username": "service@email.com",
    "password": "passwd",
    "url": "https://distrilink.odoo.com",
    "db": "hotglue",
}


def test_standard_tap_tests():
    """Run standard tap tests from the SDK."""
    tests = get_standard_tap_tests(TapOdoo, config=SAMPLE_CONFIG)
    for test in tests:
        test()
