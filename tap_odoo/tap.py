"""Odoo tap class."""

import logging
from typing import List
from xmlrpc.client import Fault

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_odoo.streams import (
    CustomerStream,
    LocationStream,
    OrderStream,
    ProductStream,
    ProductUomStream,
    PurchaseLineStream,
    PurchaseStream,
    SaleOrderLineStream,
    StockStream,
    SupplierStream,
    WarehouseStream,
    CompaniesStream,
    UsersStream,
    AccountsStream,
    InvoicesStream,
    BomStream,
    BomLinesStream,
    InvoiceLinesStream,
    InvoiceLineAllStream,
    StockMoveStream,
)

STREAM_TYPES = [
    CustomerStream,
    ProductStream,
    OrderStream,
    SaleOrderLineStream,
    PurchaseStream,
    PurchaseLineStream,
    StockStream,
    WarehouseStream,
    LocationStream,
    ProductUomStream,
    SupplierStream,
    CompaniesStream,
    UsersStream,
    AccountsStream,
    InvoicesStream,
    BomStream,
    BomLinesStream,
    InvoiceLinesStream,
    InvoiceLineAllStream,
    StockMoveStream,
]


class TapOdoo(Tap):
    """Odoo tap class."""

    name = "tap-odoo"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "url",
            th.StringType,
            required=True,
        ),
        th.Property(
            "db",
            th.StringType,
            required=True,
        ),
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
        th.Property(
            "start_date",
            th.DateTimeType,
        ),
        th.Property("page_size", th.IntegerType, default=100),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        streams = []
        for stream_class in STREAM_TYPES:
            try:
                streams.append(stream_class(tap=self))
            except Fault as e:
                if stream_class.endpoint in str(e):
                    logging.warning(f"Stream {stream_class.name} not found in account.")
                else:
                    raise e
        return streams


if __name__ == "__main__":
    TapOdoo.cli()
