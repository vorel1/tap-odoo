"""Custom client handling, including OdooStream base class."""

import xmlrpc.client
from http.client import ResponseNotReady
from typing import Any, Dict, Iterable, List, Optional, Union

import backoff
from backports.cached_property import cached_property
from singer_sdk import typing as th
from singer_sdk.streams import Stream


class OdooStream(Stream):
    ignore_incremental_sync = ["bom","bom_lines"]

    @cached_property
    def ignore_incremental_sync_streams(self):
        return self.config.get("full_sync_streams",[])
    
    @cached_property
    def _page_size(self):
        return int(self.config["page_size"])

    @cached_property
    def password(self):
        return str(self.config["password"])

    @cached_property
    def username(self):
        return str(self.config["username"])

    @cached_property
    def db(self):
        return str(self.config["db"])

    @cached_property
    def url(self):
        return str(self.config["url"])

    def uid(self):
        common = xmlrpc.client.ServerProxy("{}/xmlrpc/2/common".format(self.url))
        uid = common.authenticate(self.db, self.username, self.password, {})
        return uid

    def models(self):
        models = xmlrpc.client.ServerProxy("{}/xmlrpc/2/object".format(self.url))
        return models

    @backoff.on_exception(
        backoff.expo,
        (OverflowError, ResponseNotReady,xmlrpc.client.ProtocolError),
        max_tries=8,
        factor=3,
    )
    def query_odoo(self, uid, models, offset):
        selection = {
            "fields": self.selected_properties,
            "offset": offset,
            "limit": self._page_size,
        }
        filters = [[]]
        #Based on config flag ignore streams configured
        apply_replication = True
        if self.name in self.ignore_incremental_sync_streams:
            apply_replication = False
        #Check if stream has additional filters    
        if hasattr(self,"filters"):
            if self.filters:
                filters[0].append(self.filters)
        if self.replication_key and apply_replication:
            start_date = self.get_starting_timestamp({})
            if start_date:
                date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
                filters = [[["write_date", ">", date]]]
        try:
            return models.execute_kw(
                self.db,
                uid,
                self.password,
                self.endpoint,
                "search_read",
                filters,
                selection,
            )
        except:
            filtered_selection = [
                p
                for p in self.selected_properties
                if p not in [
                    "product_qty",
                    "credit_balance",
                    "santos_uom_ids",
                    "forecasted_issue",
                    "paid_amount",
                    "amount_paid_percent",
                    "message_channel_ids"
                ]
            ]
            selection = {
                "fields": filtered_selection,
                "offset": offset,
                "limit": self._page_size,
            }
            return models.execute_kw(
                self.db,
                uid,
                self.password,
                self.endpoint,
                "search_read",
                filters,
                selection,
            )

    @cached_property
    def selected_properties(self):
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)
        return selected_properties

    def parse_response(self, record):
        for name, field in record.items():
            if self.schema["properties"].get(name):
                type = self.schema["properties"][name].get("type")[0]
                if field == False and type != "boolean":
                    record[name] = None
                elif type == "object" and isinstance(field, list) and len(field) == 2:
                    if isinstance(field[0], int) and isinstance(field[1], str):
                        record[name] = dict(id=field[0], name=field[1])
        return record

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects."""
        # common.version()
        uid = self.uid()
        models = self.models()
        offset = 0

        while True:
            docs = self.query_odoo(uid, models, offset)
            if len(docs) == 0:
                break
            for record in docs:
                record = self.parse_response(record)
                yield record
            offset = offset + self._page_size

    @staticmethod
    def extract_type(field):
        field_type = field.get("type")
        if field_type in ["char", "date", "text", "selection", "html"]:
            return th.StringType
        if field_type in ["one2many", "many2many"]:
            return th.ArrayType(th.IntegerType)
        if field_type in ["many2one"]:
            return th.ObjectType(
                th.Property("id", th.IntegerType), th.Property("name", th.StringType)
            )
        if field_type == "integer":
            return th.IntegerType
        if field_type in ["float", "monetary"]:
            return th.NumberType
        if field_type == "datetime":
            return th.DateTimeType
        if field_type == "boolean":
            return th.BooleanType
        return None

    @cached_property
    def schema(self):
        properties = []
        uid = self.uid()
        models = self.models()
        fields = models.execute_kw(
            self.db,
            uid,
            self.password,
            self.endpoint,
            "fields_get",
            [],
            {"attributes": ["type"]},
        )

        for field_name, types in fields.items():
            type = self.extract_type(types)
            property = th.Property(field_name, type)
            if type:
                properties.append(property)

        return th.PropertiesList(*properties).to_dict()
